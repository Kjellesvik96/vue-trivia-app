import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Menu from './components/Menu'
import Quiz from './components/Quiz'
import Result from './components/Result'

const routes = [
      { path: '/', component: Menu },
      { path: '/questions/:categoryId', component: Quiz },
      { path: '/result',name: 'result', 
      props: (route) => ({
        ...route.params
      }),
      component: Result }
    ]

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
