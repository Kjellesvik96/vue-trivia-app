const BASE_URL = "https://opentdb.com/api.php?amount=10&difficulty=easy&type=multiple&category";

export const CATEGORIES = {
    SPORT: 21,
    ANIMAL: 27,
    VEHICLE: 28,
    FILM: 11
}

export const getQuestions = (categoryId) => {
    return fetch(`${BASE_URL}=${categoryId}`)
                        .then(response => response.json())
                        .then(response => response.results);
}